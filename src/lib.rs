#[macro_use(quickcheck)]
extern crate quickcheck_macros;

use itertools::*;
use std::cmp::max;
use std::cmp::Ordering;
use std::collections::BTreeSet;
use std::iter::FromIterator;

#[derive(Debug, PartialEq, Clone)]
struct AVLNode<T: Ord> {
    value: T,
    left: AVLTree<T>,
    right: AVLTree<T>,
    height: usize,
}

impl<T: Ord> AVLNode<T> {
    fn new(value: T) -> AVLTree<T> {
        Some(Box::new(AVLNode {
            value: value,
            left: None,
            right: None,
            height: 1,
        }))
    }

    fn update_height(&mut self) {
        self.height = self.height();
    }

    fn height(&self) -> usize {
        1 + max(
            self.left.as_ref().map_or(0, |left_node| left_node.height()),
            self.right
                .as_ref()
                .map_or(0, |right_node| right_node.height()),
        )
    }

    fn left_height(&self) -> usize {
        self.left.as_ref().map_or(0, |left_node| left_node.height())
    }

    fn right_height(&self) -> usize {
        self.right
            .as_ref()
            .map_or(0, |right_node| right_node.height())
    }

    fn balance_factor(&self) -> i8 {
        let left_height = self.left_height();
        let right_height = self.right_height();

        if left_height >= right_height {
            (left_height - right_height) as i8
        } else {
            -((right_height - left_height) as i8)
        }
    }
}

type AVLTree<T> = Option<Box<AVLNode<T>>>;

#[derive(Debug, PartialEq, Clone)]
struct AVLTreeSet<T: Ord> {
    root: AVLTree<T>,
}

impl<T: Ord> AVLTreeSet<T> {
    fn new() -> Self {
        Self { root: None }
    }

    fn insert(&mut self, value: T) -> bool {
        let mut current_tree = &mut self.root;

        while let Some(current_node) = current_tree {
            match current_node.value.cmp(&value) {
                Ordering::Less => current_tree = &mut current_node.right,
                Ordering::Greater => current_tree = &mut current_node.left,
                Ordering::Equal => {
                    return false;
                }
            }
        }

        *current_tree = AVLNode::new(value);
        true
    }

    fn iter(&self) -> AVLTreeSetIter<T> {
        AVLTreeSetIter {
            prev_nodes: vec![],
            current_tree: &self.root,
        }
    }
}

#[derive(Debug)]
struct AVLTreeSetIter<'a, T: Ord> {
    prev_nodes: Vec<&'a AVLNode<T>>,
    current_tree: &'a AVLTree<T>,
}

impl<'a, T: 'a + Ord> Iterator for AVLTreeSetIter<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            match *self.current_tree {
                Some(ref node) => {
                    // moving to the left possible?
                    if node.left.is_some() {
                        self.prev_nodes.push(&node);
                        self.current_tree = &node.left;
                        continue;
                    }

                    // no more left, return this value.
                    // but before returning this value,
                    // make the right tree (if available) the current_tree, so that it can
                    // be traversed in the next iteration
                    self.current_tree = &node.right;
                    return Some(&node.value);
                }

                None => match self.prev_nodes.pop() {
                    Some(ref node) => {
                        self.current_tree = &node.right;
                        return Some(&node.value);
                    }
                    None => return None,
                },
            }
        }
    }
}

impl<T: Ord> FromIterator<T> for AVLTreeSet<T> {
    fn from_iter<I: IntoIterator<Item = T>>(iter: I) -> Self {
        let mut set = Self::new();

        for item in iter {
            set.insert(item);
        }

        set
    }
}

#[cfg(test)]
mod properties {
    use super::*;
    use std::collections::BTreeSet;

    #[quickcheck]
    fn qc_iterator_parity(xs: Vec<usize>) -> bool {
        let avl_set = xs.iter().cloned().collect::<AVLTreeSet<_>>();
        let btree_set = xs.iter().cloned().collect::<BTreeSet<_>>();

        itertools::equal(avl_set.iter(), btree_set.iter())
    }

    #[quickcheck]
    fn qc_insert_parity(mut btree_set: BTreeSet<u8>, x: u8) -> bool {
        let mut avl_set = btree_set.iter().cloned().collect::<AVLTreeSet<_>>();
        avl_set.insert(x) == btree_set.insert(x)
    }
}

#[cfg(test)]
mod tests {
    use super::{AVLNode, AVLTreeSet};

    #[test]
    fn test_iterator() {
        let mut set = AVLTreeSet::new();

        for i in (1..4 as usize).rev() {
            set.insert(i);
        }

        let mut iter = set.iter();
        assert_eq!(iter.next(), Some(&1));
        assert_eq!(iter.next(), Some(&2));
        assert_eq!(iter.next(), Some(&3));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_insert() {
        let mut set = AVLTreeSet::new();

        assert!(set.insert(1)); // Insert new value
        assert!(!set.insert(1)); // Should not insert existing value

        assert!(set.insert(2)); // Insert another new value
        assert_eq!(
            // Checking the tree structure
            set.root,
            Some(Box::new(AVLNode {
                value: 1,
                left: None,
                right: Some(Box::new(AVLNode {
                    value: 2,
                    left: None,
                    right: None,
                    height: 1
                })),
                height: 2
            }))
        );
    }
}
